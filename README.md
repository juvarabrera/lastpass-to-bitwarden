# lastpass-to-bitwarden

An alternative tool in migrating your passwords and notes from LastPass to Bitwarden. This tool is not about Bitwarden being a better password manager than LastPass. This is just a tool if anyone needs to migrate their data that includes different features that LastPass does not include.

Note: This tool would also work on Vaultwarden, a Bitwarden alternative.

## Why this tool?

I know there is an easier way to export data from LastPass with a single click and getting a `.csv` file and importing it to your BitWarden account. Through my experience, I have encountered some inconveniences when exporting and importing my data -- they are not that consistent. Here are some of the reasons why I made this tool:

### Properly exports attributes of notes
Attributes of LastPass notes are exported in one single Notes field. It is harder to select a specific value from one huge textarea. For example, an `ssh key` item from LastPass will have multiple attributes like the `Bit Strength`, `Format`, `Passphrase`, `Public Key`, `Private Key`, and other details. When exported it will look like this:

```
Bit Strength: 4096
Format: rsa
Passphrase: password
Public Key: ...
Private Key: ...
...
```

And when you import it to BitWarden, it is possible that it would be unsuccessful if you have your public and private key attached as BitWarden only allows 10,000 characters in a note. If the import is successful, it would still be an inconvenience if you had to manually select your passphrase from the textbox and copy it. 

In addition, the Passphrase would be imported in the `Notes` field instead of a `Password` field so that you can easily, copy paste it.

### Properly exports multiple Identities from LastPass to Bitwarden's Organization
LastPass does not export identities data. If you import it to other password managers, your data would be unorganize. Bitwarden, on the otherhand, when exported, it includes the organization and collections data. This tool would organize your LastPass data categorized by identities/organizations.

### Synchronize "Require master password reprompt" attribute
LastPass does not export if a specific note or password should be opened with a master password prompt or not.


### Retains folders of items inside organizations
When importing data to organization, it does not include the folders making it hard to organize your items inside Organizations. To bypass this, a record must be created in the main vault then move the item to the appropriate organization to retain the folder of the item.

## How to Use

### 1. Run `lastpass-export.js` script
1. Open your **LastPass vault** in your browser tab
  a. Open LastPass vault in your browser extension
  b. Select Open My Vault
2. Open **Dev Tools** `Ctrl + Shift + I` or `F12`
3. Go to `Console`
4. Run the script
5. It will ask for your master password to open protected records and to switch between identities **BUT DON'T WORRY**, your password is not transferred to any APIs or anything. You can check the script yourself. 

It will take time to get all the data. At the end of the process, you should be able to download `lastpass-to-bitwarden.json` file automatically.

### 2. Import the JSON file to Bitwarden Vault
1. Go to your Bitwarden Vault
2. Go to `Tools`
3. Select `Import Data`
4. Select `Bitwarden (json)` for the format
5. Select the `lastpass-to-bitwarden.json` file for the import file OR paste the json file contents in the textarea.
6. Click `Import data` button

### 3. Run `bitwarden-after-import.js` script
1. Go to your Bitwarden vault in your browser tab
2. Open **Dev Tools** `Ctrl + Shift + I` or  `F12`
3. Go to `Console`
4. Run the script

#### 4. You're Done

## Works On
1. LastPass version 4.106.1
2. Bitwarden/Vaultwarden Version 2022.12.0

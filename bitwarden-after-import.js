/*
    This script is executed after importing the data.
*/

const ENVIRONMENT = {
    PAUSE_SECOND: 1,
    INTERVAL_MAX_TRIES: 60,
    INTERVAL_MILLISECONDS: 200
}

class BitwardenAfterImport {
    
    constructor() {
        this.organizations = {};
    }

    getProgress(count, total, time) {
        time /= 1000;
        let secondsRemaining = Math.floor(time * ((total+1)-(count+1)));
        let minutesRemaining = 0;
        if(secondsRemaining >= 60) {
            minutesRemaining = Math.floor(secondsRemaining / 60);
            secondsRemaining -= minutesRemaining * 60;
        }
        console.log((minutesRemaining != 0 ? minutesRemaining + " minutes " : "") + secondsRemaining + " seconds remaining...")
    }

    loadAllItems() {
        return new Promise(async resolve => {
            while(true) {
                let initialHeight = document.querySelector("body").scrollHeight;
                window.scrollTo(0,document.body.scrollHeight);
                await pause();
                let afterHeight = document.querySelector("body").scrollHeight;
                if(initialHeight == afterHeight) break;
            }
            window.scrollTo(0,0);
            resolve(true);
        });
    }
    
    run() {
        let obj = this;
        return new Promise(async resolve => {
            await this.loadAllItems();
            let items = document.querySelectorAll("table tr a");
            for(let i = 0; i < items.length; i++) {
                let start = new Date().getTime();
                console.log("Progress: " + (i+1) + "/" + (items.length+1) + " = " + (((i+1)/(items.length+1)) * 100).toFixed(2) + "%");
                let htmlA = items[i];
                let itemId = htmlA.attributes.href.value.split("=")[1];
                htmlA.click();
                let popup = new Popup();
                await popup.isOpen();
                let organization = await popup.getOrganizationData();
                if(organization != null) {
                    if(!obj.organizations.hasOwnProperty(organization.name)) {
                        obj.organizations[organization.name] = new Organization(organization.name);
                        obj.organizations[organization.name].lastpassExportId = organization.exportId;
                    }   
                    obj.organizations[organization.name].items.push(new Item(itemId));
                }
                popup.close();
                await pause();
                let end = new Date().getTime();
                obj.getProgress(i+1, items.length, end-start);
            }
            console.log(obj.organizations);
            for(const [organizationName, organization] of Object.entries(obj.organizations)) {
                window.location = "/#/create-organization";
                await pause();
                document.querySelector(`[formcontrolname="name"]`).value = organization.name;
                document.querySelector(`[formcontrolname="name"]`).dispatchEvent(new Event("input"));

                document.querySelector(`[formcontrolname="billingEmail"]`).value = "sample-email@changethis.com";
                document.querySelector(`[formcontrolname="billingEmail"]`).dispatchEvent(new Event("input"));
                
                document.querySelector(`[buttontype="primary"]`).removeAttribute("disabled")
                document.querySelector(`[buttontype="primary"]`).click();
                await this.isOrganizationVaultLoaded();
                organization.id = window.location.hash.split("/")[2];

                window.location = "/#/vault";
                await this.isVaultLoaded();
                document.querySelector("#search").value = organization.lastpassExportId;
                document.querySelector("#search").dispatchEvent(new Event("input"));
                await pause(5000);
                document.querySelector("#bulkActionsButton").click();
                document.querySelector(`[aria-labelledby="bulkActionsButton"]`).querySelectorAll("button")[3].click();
                document.querySelector("#bulkActionsButton").click();
                document.querySelector(`[aria-labelledby="bulkActionsButton"]`).querySelectorAll("button")[1].click();
                await pause();
                document.querySelector("#organization").value = document.querySelector(`#organization option[value*='${organization.id}']`).value;
                document.querySelectorAll("app-vault-bulk-share button")[1].click();
                document.querySelectorAll("app-vault-bulk-share button")[3].click();
                // for(let i = 0; i < organization.items.length; i++) {
                //     let item = organization.items[i];
                //     document.querySelector(`a[href="#/vault?itemId=${item.id}"]`).parentNode.parentNode.querySelector("button[title='Options']").click();
                //     document.querySelector(".cdk-overlay-container i.bwi-arrow-circle-right").parentNode.click();
                //     await pause();
                //     document.querySelector("#organization").value = document.querySelector(`#organization option[value*='${organization.id}']`).value;
                //     document.querySelectorAll("app-vault-share button")[1].click();
                //     document.querySelectorAll("app-vault-share button")[3].click();
                //     await pause();
                // }
                await pause(5000);
            }
            resolve();
        });
    }

    isVaultLoaded() {
        let obj = this;
        return new Promise((resolve, reject) => {
            let tries = 0;
            let i = setInterval(() => {
                if(tries == ENVIRONMENT.INTERVAL_MAX_TRIES) reject(Popup.OPEN_ERROR_MESSAGE);
                if(document.querySelector("app-vault-ciphers") != null) {
                    if(document.querySelectorAll("app-vault-ciphers tr").length > 0) {
                        clearInterval(i);
                        resolve(true);
                    }
                }
                tries++;
            }, ENVIRONMENT.INTERVAL_MILLISECONDS);
        });
    }

    isOrganizationVaultLoaded() {
        let obj = this;
        return new Promise((resolve, reject) => {
            let tries = 0;
            let i = setInterval(() => {
                if(tries == ENVIRONMENT.INTERVAL_MAX_TRIES) reject(Popup.OPEN_ERROR_MESSAGE);
                if(document.querySelector("app-org-vault-ciphers") != null) {
                    clearInterval(i);
                    resolve(true);
                }
                tries++;
            }, ENVIRONMENT.INTERVAL_MILLISECONDS);
        });
    }
}

class Organization {
    constructor(name) {
        this.id = null;
        this.lastpassExportId = null;
        this.name = name;
        this.items = [];
    }
}


class Popup {
    static OPEN_ERROR_MESSAGE = "Unable to open popup";
    constructor() {

    }
    getOrganizationData() {
        return new Promise(resolve => {
            let bitwardenOrganizationInput = document.querySelector(".modal-dialog").querySelector("[role=group][aria-label=\"bitwarden-organization-data\"]");
            if(bitwardenOrganizationInput == null)
                resolve(null);
            resolve({
                "name": bitwardenOrganizationInput.querySelectorAll("input")[1].value,
                "exportId": document.querySelector(".modal-dialog").querySelector("[role=group][aria-label=\"bitwarden-organization-id\"]").querySelectorAll("input")[1].value
            });
        });
    }
    isOpen() {
        let obj = this;
        return new Promise((resolve, reject) => {
            let tries = 0;
            let i = setInterval(() => {
                if(tries == ENVIRONMENT.INTERVAL_MAX_TRIES) reject(Popup.OPEN_ERROR_MESSAGE);
                if(document.querySelector(".modal-dialog") != null) {
                    clearInterval(i);
                    resolve(true);
                }
                tries++;
            }, ENVIRONMENT.INTERVAL_MILLISECONDS);
        });
    }
    close() {
        document.querySelector(".modal-dialog button.close").click();
    }
}

class Item {
    constructor(id) {
        this.id = id;
    }
}

function pause(second) {
    return new Promise(resolve => {
        setTimeout(function() {
            resolve();
        }, second != null ? second : ENVIRONMENT.PAUSE_SECOND * 1000)
    });
}

let bitwardenAfterImport = new BitwardenAfterImport();
await bitwardenAfterImport.run();

/*

Script is work in progress! Do not use it yet.

*/

const ENVIRONMENT = {
    // Maximum number of tries to check if the condition is true
    INTERVAL_MAX_TRIES: 20,

    // Interval seconds
    INTERVAL_MILLISECONDS: 200,

    // Number of seconds to pause before moving on to the next command
    PAUSE_SECOND: 1,

    /*  If a record is password enabled, the script can enable
        the "Do not reprompt" to prevent asking for passwords 
        again and again.
    */
    ENABLE_DO_NOT_RE_PROMPT_MASTER_PASSWORD: true,

    /*  If ENABLE_DO_NOT_RE_PROMPT_MASTER_PASSWORD is true,
        enter the number of seconds when to reprompt next
        
        Accepted values are the following:
            30      for 30 seconds
            60      for 60 seconds
            300     for 5 minutes
            900     for 15 minutes
            1800    for 30 minutes
            3600    for 1 hour
            10800   for 3 hours
            21600   for 6 hours
            28800   for 8 hours
            43200   for 12 hours
            86400   for 24 hours

        This script usually runs a second PER record.
        If you have 100 records, you would have ~100 seconds
        runtime. So a value of 300 is appropriate.
        You can always set it to higher timeframe.
    */
    DO_NOT_RE_PROMPT_DURATION: 300
}


function getNode(selector) {
    return document.querySelector(selector);
}
function getNodes(selector) {
    return document.querySelectorAll(selector);
}
class BitwardenData {
    constructor() {
        this.items = {};
        this.organizations = {};
        this.folders = {};
    }
    getFolder(folderName) {
        if(folderName == "") return null;
        if(this.folders.hasOwnProperty(folderName))
            return this.folders[folderName];
        return this.createFolder(folderName);
    }
    createFolder(folderName) {
        return this.folders[folderName] = new BitwardenFolder(folderName);
    }
}
class LastPassExport {
    // Record tile in vault page
    static RECORD_TILE = ".folderWrapper .tile";

    static USER_MENU_CONTAINER = "#userMenuContainer";

    static IDENTITIES_CONTAINER = "#identityDropdownContainer";

    static ALL_IDENTITY = "All";

    static MANAGE_IDENTITIES_BUTTON = "#manageIdentitiesMenuItem";

    static IDENTITY_LAUNCH_BUTTON = ".list[aria-label=\"Identity: {identityName}\"] button.launchButton"

    static ALL_ITEMS_BUTTON = "#all";

    constructor() {
        this.identities = [];
        this.masterPassword = null;
    }
    getAllIdentities() {
        let x = [];
        getNode(LastPassExport.IDENTITIES_CONTAINER).querySelectorAll("li").forEach(li => {
            if(li.textContent == LastPassExport.ALL_IDENTITY) return;
            x.push(new LastPassIdentity(li.textContent));
        });
        return x;
    }
    switchIdentity(identity, masterPassword) {
        return new Promise(async resolve => {
            console.log("Accessing Identity: " + identity.name);
            getNode(LastPassExport.MANAGE_IDENTITIES_BUTTON).click();
            await pause(100);
            getNode(LastPassExport.IDENTITY_LAUNCH_BUTTON.replace("{identityName}", identity.name)).click();
            await pause(100);
            let passwordReprompt = new PasswordReprompt();
            if(passwordReprompt.isVisible())
            passwordReprompt.enterPassword(masterPassword);
            await pause(100);
            getNode(LastPassExport.ALL_ITEMS_BUTTON).click();
            await pause(2000);
            resolve();
        });
    }
    askForMasterPassword() {
        return prompt("What is your master password? This is to open records that are password protected and to switch between identities.");
    }
    getPopupObject() {
        if(getNode(PasswordPopup.CONTAINER_SELECTOR).classList.contains(PasswordPopup.CLASS_IF_VISIBLE)) {
            return new PasswordPopup();
        } else {
            return new NotePopup();
        }
    }
    static getMonthNumber(monthName) {
        const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        if(months.indexOf(monthName) >= 0)
            return (months.indexOf(monthName) + 1) + "";
        return "";
    }
    static getBitwardenRecordType(lastpassRecordType) {
        if([
            LastPassRecord.TYPE.PASSWORD,
            LastPassRecord.TYPE.MEMBERSHIP,
            LastPassRecord.TYPE.WIFI_PASSWORD,
            LastPassRecord.TYPE.EMAIL_ACCOUNT,
            LastPassRecord.TYPE.INSTANT_MESSENGER,
            LastPassRecord.TYPE.DATABASE,
            LastPassRecord.TYPE.SERVER,
            LastPassRecord.TYPE.SSH_KEY
        ].indexOf(lastpassRecordType) >= 0) {
            return BitwardenRecord.TYPE.LOGIN;
        } else if([
            LastPassRecord.TYPE.PAYMENT_CARD
        ].indexOf(lastpassRecordType) >= 0) {
            return BitwardenRecord.TYPE.CARD;
        } else if([
            LastPassRecord.TYPE.ADDRESS
        ].indexOf(lastpassRecordType) >= 0) {
            return BitwardenRecord.TYPE.IDENTITY;
        } else {
            return BitwardenRecord.TYPE.SECURE_NOTE;
        }
    }
    getProgress(count, total, time) {
        time /= 1000;
        let secondsRemaining = Math.floor(time * ((total+1)-(count+1)));
        let minutesRemaining = 0;
        if(secondsRemaining >= 60) {
            minutesRemaining = Math.floor(secondsRemaining / 60);
            secondsRemaining -= minutesRemaining * 60;
        }
        console.log((minutesRemaining != 0 ? minutesRemaining + " minutes " : "") + secondsRemaining + " seconds remaining...")
    }
    async run() {
        let bitwardenData = new BitwardenData();
        let masterPassword = this.askForMasterPassword();
        let records = getNodes(LastPassExport.RECORD_TILE);
        for(let i = 0; i < records.length; i++) {
            let start = new Date().getTime();
            console.log("Progress: " + (i+1) + "/" + (records.length+1) + " = " + (((i+1)/(records.length+1)) * 100).toFixed(2) + "%");
            let record = new LastPassRecordTile(records[i]);
            record.open();
            let id = record.getId();
            await pause(500);
            if(record.isPasswordPrompt()) {
                let passwordReprompt = new PasswordReprompt();
                await pause(100);
                passwordReprompt.enterPassword(masterPassword);
                await pause(100);
            }
            let popup = this.getPopupObject();
            
            await popup.isOpen();
            let lastpassRecord = new LastPassRecord(id, popup.getData(), popup.getType());
            let bitwardenRecord = new BitwardenRecord();
            bitwardenRecord.setFolder(bitwardenData.getFolder(lastpassRecord.folder));
            bitwardenRecord.convertLastPassRecord(lastpassRecord);
            bitwardenData.items[id] = bitwardenRecord;
            popup.close();
            let end = new Date().getTime();
            this.getProgress(i+1, records.length, end-start);
        }
        this.identities = this.getAllIdentities();
        for(let i = 0; i < this.identities.length; i++) {
            let identity = this.identities[i];
            bitwardenData.organizations[identity.name] = new BitwardenOrganization(identity.name);
            let organization = bitwardenData.organizations[identity.name];
            await this.switchIdentity(identity, masterPassword);
            records = getNodes(LastPassExport.RECORD_TILE);
            for(let i = 0; i < records.length; i++) {
                let record = new LastPassRecordTile(records[i]);
                let id = record.getId();
                if(!bitwardenData.items.hasOwnProperty(id)) continue;
                let item = bitwardenData.items[id];
                item.setOrganization(organization);
            }
        }
        await this.switchIdentity({"name": LastPassExport.ALL_IDENTITY}, masterPassword);
        await this.export(bitwardenData);
        console.log("Done! Import the .json file in Bitwarden > Tools > Import data.");
    }
    async export(bitwardenData) {
        return new Promise(async resolve => {
            console.log("Exporting...");
            let exportData = {
                "encrypted": false,
                "folders": [],
                "items": []
            }
            for (const [key, folder] of Object.entries(bitwardenData.folders)) {
                exportData.folders.push(folder);
            }
            for (const [key, item] of Object.entries(bitwardenData.items)) {
                exportData.items.push(item);
            }
            var element = document.createElement('a');
            element.setAttribute('href', 'data:text/json;charset=utf-8,' + encodeURIComponent(JSON.stringify(exportData, null, 2)));
            element.setAttribute('download', "lastpass-to-bitwarden.json");
            element.style.display = 'none';
            document.body.appendChild(element);
            element.click();
            document.body.removeChild(element);
            resolve();
        });
    }
}

class LastPassIdentity {
    constructor(name) {
        this.name = name;
    }
}

class BitwardenOrganization {
    constructor(name) {
        this.id = crypto.randomUUID();
        this.name = name;
        // this.items = [];
    }
}

class PasswordReprompt {
    static DIALOG = "#repromptDialog";
    static PASSWORD_FIELD = "#loginDialogPassword";
    static CHECKBOX = "#repromptDialogRepromptTime";
    static DURATION_DROPDOWN = "#repromptDialog select";
    static CONTINUE_BUTTON = "#repromptDialog button[automation-id='okButton']";
    constructor() {

    }
    isVisible() {
        let repromptDialog = getNode(PasswordReprompt.DIALOG);
        if(repromptDialog == null) return false;
        return repromptDialog.style.display == "block";
    }
    enterPassword(masterPassword) {
        getNode(PasswordReprompt.PASSWORD_FIELD).value = masterPassword;
        if(ENVIRONMENT.ENABLE_DO_NOT_RE_PROMPT_MASTER_PASSWORD) {
            getNode(PasswordReprompt.CHECKBOX).checked = true;
            getNode(PasswordReprompt.DURATION_DROPDOWN).value = ENVIRONMENT.DO_NOT_RE_PROMPT_DURATION;
            getNode(PasswordReprompt.CONTINUE_BUTTON).click();
        }
    }
}

class LastPassRecord {
    static TYPE = {
        "PASSWORD": "password",
        "SECURE_NOTE": "secure note",
        "ADDRESS": "address",
        "PAYMENT_CARD": "payment card",
        "BANK_ACCOUNT": "bank account",
        "DRIVERS_LICENSE": "driver's license",
        "PASSPORT": "passport",
        "SOCIAL_SECURITY_NUMBER": "social security number",
        "HEALTH_INSURANCE_POLICY": "health insurance",
        "MEMBERSHIP": "membership",
        "WIFI_PASSWORD": "wi-fi password",
        "EMAIL_ACCOUNT": "email account",
        "INSTANT_MESSENGER": "instant messenger",
        "DATABASE": "database",
        "SERVER": "server",
        "SSH_KEY": "ssh key",
        "SOFTWARE_LICENSE": "software license"
    }

    constructor(id, data, recordType) {
        this.id = id;
        this.data = data;
        this.recordType = recordType;
        this.name = this.data.name;
        delete this.data.name;
        this.folder = recordType == LastPassRecord.TYPE.PASSWORD ? this.data.folder : this.data.group;
        this.folder.replace("\\","/");
        delete this.data.group;
        delete this.data.folder;
        this.passwordProtected = this.data.passwordProtected;
        delete this.data.passwordProtected;
        this.favorite = this.data.favorite;
        delete this.data.favorite;
        this.Notes = null;
        if(this.recordType == LastPassRecord.TYPE.PASSWORD) {
            this.Notes = this.data.notes;
            delete this.data.notes;
            delete this.data.autoLogin;
            delete this.data.autoFill;
        }
        if(this.data.hasOwnProperty(NotePopup.DEFAULT_LABEL)) {
            this.Notes = this.data.Notes;
            delete this.data.Notes;
        }
    }
    getThenDeleteData(name) {
        let tempValue = this.data[name];
        delete this.data[name];
        return tempValue;
    }
    deleteData(names) {
        let obj = this;
        names.forEach(name => {
            delete obj.data[name];
        })
    }
}
class BitwardenPasswordUri {
    constructor(match = null, uri = null) {
        this.match = match;
        this.uri = uri;
    }
}

class BitwardenRecord {
    static TYPE = {
        LOGIN: 1,
        SECURE_NOTE: 2,
        CARD: 3,
        IDENTITY: 4
    }
    constructor() {
        this.id = crypto.randomUUID();
        this.organizationId = null;
        this.folderId = null;
        this.fields = [];
        this.reprompt = 0;
        this.name = "";
        this.notes = null;
        this.favorite = false;
        this.collectionIds = null;
        this.type = null;
    }

    setOrganization(organization) {
        this.fields.push(new BitwardenCustomField("bitwarden-organization-data", organization.name));
        this.fields.push(new BitwardenCustomField("bitwarden-organization-id", organization.id));
        this.organizationId = organization.id;
    }

    convertLastPassRecord(lastpassRecord) {
        this.type = LastPassExport.getBitwardenRecordType(lastpassRecord.recordType);
        this.name = lastpassRecord.name;
        this.favorite = lastpassRecord.favorite;
        this.notes = lastpassRecord.Notes;
        if(this.type == BitwardenRecord.TYPE.LOGIN) {
            this.login = {
                "uris": [],
                "username": lastpassRecord.getThenDeleteData("username"),
                "password": lastpassRecord.getThenDeleteData("password"),
                "totp": null
            }
            if(lastpassRecord.data.hasOwnProperty("loginUrl"))
                this.login.uris.push(new BitwardenPasswordUri(null, lastpassRecord.getThenDeleteData("loginUrl")));
        } else if(this.type == BitwardenRecord.TYPE.CARD) {
            let expirationData = lastpassRecord.getThenDeleteData("Expiration Date");
            this.card = {
                "cardholderName": lastpassRecord.getThenDeleteData("Name on Card"),
                "brand": null,
                "number": lastpassRecord.getThenDeleteData("Number"),
                "expMonth": LastPassExport.getMonthNumber(expirationData["month"]),
                "expYear": expirationData["year"],
                "code": lastpassRecord.getThenDeleteData("Security Code")
            }
        } else if(this.type == BitwardenRecord.TYPE.IDENTITY) {
            let title = lastpassRecord.getThenDeleteData("Title");
            let phone = lastpassRecord.getThenDeleteData("Phone");
            this.identity = {
                "title": title.charAt(0).toUpperCase() + title.slice(1),
                "firstName": lastpassRecord.getThenDeleteData("First Name"),
                "middleName": lastpassRecord.getThenDeleteData("Middle Name"),
                "lastName": lastpassRecord.getThenDeleteData("Last Name"),
                "address1": lastpassRecord.getThenDeleteData("Address 1"),
                "address2": lastpassRecord.getThenDeleteData("Address 2"),
                "address3": lastpassRecord.getThenDeleteData("Address 3"),
                "city": lastpassRecord.getThenDeleteData("City/Town"),
                "state": lastpassRecord.getThenDeleteData("State"),
                "postalCode": lastpassRecord.getThenDeleteData("ZIP/Postal Code"),
                "country": lastpassRecord.getThenDeleteData("Country"),
                "company": lastpassRecord.getThenDeleteData("Company"),
                "email": lastpassRecord.getThenDeleteData("Email Address"),
                "phone": (phone["countryCode"] != "" ? "+" + phone["countryCode"] : "") + phone["phone"] + (phone["ext"] != "" ? " ext. " + phone["ext"] : ""),
                "ssn": null,
                "username": lastpassRecord.getThenDeleteData("Username"),
                "passportNumber": null,
                "licenseNumber": null
            }
        } else {
            this.type = 2;
            this.secureNote = {
                "type": 0,
            }
        }
        if(lastpassRecord.recordType == LastPassRecord.TYPE.MEMBERSHIP) {
            this.login = {
                "uris": [],
                "username": lastpassRecord.getThenDeleteData("Membership Number"),
                "password": lastpassRecord.getThenDeleteData("Password"),
                "totp": null
            }
            this.login.uris.push(new BitwardenPasswordUri(null, lastpassRecord.getThenDeleteData("Website")));
        } else if(lastpassRecord.recordType == LastPassRecord.TYPE.WIFI_PASSWORD) {
            this.login = {
                "uris": [],
                "username": lastpassRecord.getThenDeleteData("SSID"),
                "password": lastpassRecord.getThenDeleteData("Password"),
                "totp": null
            }
        } else if(lastpassRecord.recordType == LastPassRecord.TYPE.EMAIL_ACCOUNT) {
            this.login = {
                "uris": [],
                "username": lastpassRecord.getThenDeleteData("Username"),
                "password": lastpassRecord.getThenDeleteData("Password"),
                "totp": null
            }
            this.login.uris.push(new BitwardenPasswordUri(null, lastpassRecord.getThenDeleteData("SMTP Server")));
        } else if(lastpassRecord.recordType == LastPassRecord.TYPE.INSTANT_MESSENGER) {
            this.login = {
                "uris": [],
                "username": lastpassRecord.getThenDeleteData("Username"),
                "password": lastpassRecord.getThenDeleteData("Password"),
                "totp": null
            }
            this.login.uris.push(new BitwardenPasswordUri(null, lastpassRecord.getThenDeleteData("Server")));
        } else if(lastpassRecord.recordType == LastPassRecord.TYPE.DATABASE || 
            lastpassRecord.recordType == LastPassRecord.TYPE.SERVER) {
            this.login = {
                "uris": [],
                "username": lastpassRecord.getThenDeleteData("Username"),
                "password": lastpassRecord.getThenDeleteData("Password"),
                "totp": null
            }
            this.login.uris.push(new BitwardenPasswordUri(null, lastpassRecord.getThenDeleteData("Hostname")));
        } else if(lastpassRecord.recordType == LastPassRecord.TYPE.SSH_KEY) {
            this.login = {
                "uris": [],
                "username": null,
                "password": lastpassRecord.getThenDeleteData("Passphrase"),
                "totp": null
            }
            this.login.uris.push(new BitwardenPasswordUri(null, lastpassRecord.getThenDeleteData("Hostname")));
        }
        for (let [key, value] of Object.entries(lastpassRecord.data)) {
            if(value.hasOwnProperty("country") && 
            value.hasOwnProperty("countryCode") && 
            value.hasOwnProperty("phone") &&
            value.hasOwnProperty("ext")) {
                this.fields.push(new BitwardenCustomField(key, 
                    (value["countryCode"] != "" ? "+" + value["countryCode"] : "") + value["phone"] + (value["ext"] != "" ? " ext. " + value["ext"] : "")
                ));
            } else if(value.hasOwnProperty("year") && 
            value.hasOwnProperty("month") && 
            value.hasOwnProperty("day")) {
                this.fields.push(new BitwardenCustomField(key, 
                    value["month"] + " " + value["day"] + ", " + value["year"]
                ));
            } else if(value.hasOwnProperty("year") && 
            value.hasOwnProperty("month")) {
                this.fields.push(new BitwardenCustomField(key, 
                    value["month"] + " " + value["year"]
                ));
            } else {
                this.fields.push(new BitwardenCustomField(key, value));
            }
        }
        
    }

    setFolder(folder) {
        this.folder = folder;
        if(folder != null)
            this.folderId = folder.id;
    }
}

class BitwardenCustomField {
    constructor(name = "",value = null) {
        this.name = name,
        this.value = value,
        this.type = 0;
        this.linkedId = null;
    }
}
class BitwardenFolder {
    constructor(name) {
        this.id = crypto.randomUUID();
        this.name = name;
    }
}

class LastPassRecordTile {
    static RECORD_EDIT_BUTTON = "button.edit";

    static ID_CONTAINER = ".tile-view_pw-alert-info-icon";
    constructor(record) {
        this.container = record;
    }
    open() {
        this.container.querySelector(LastPassRecordTile.RECORD_EDIT_BUTTON).click();
    }
    getId() {
        return this.container.querySelector(LastPassRecordTile.ID_CONTAINER).dataset.alertId;
    }
    isPasswordPrompt() {
        let passwordReprompt = new PasswordReprompt();
        return passwordReprompt.isVisible();
    }
}

class Popup {
    static IMAGE_SRC_CHECKBOX_CHECKED = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAXJJREFUOBFjvJGRPuHPuVO5/79/Y2IgATBycv1jMTKbzHjFzPAvqZph9oAMYSJXM8gQkF6SnA3SxCIuzsAsJAxiggFJBrBKSzMoLVgKxEsYmDi5SDOATUERqHEZA5u0DMO71SsZ/n3/BjaABeIQ/CS7qhqD4qx5YKc/a2lgeLdqBVwD3AvieYUMEmWVcAkYg0NLm0Fx7kIGZkEhhqd1VSiaQWrALmDk5GTgsbJh4AQqZuLgYHjW3AAM4v8MnHoGDArTZwP9y8nwpLyY4ePO7TBz4TRzlrhIA8OfPwwfd2xj4DYxY+BzcmFglZFl+PftK4P81JkMjOxsDI9L8hk+7dkN14TMYLysq/4fJgAKWfmpM8AGgcT+/fjB8Kggh+HLsSMwJRg0PAzAGoAh+yAzleHz0cMMf4EueJiVhlczSA+KC2DGM7KyMoCi7eftWzAhnDSKC2Cq/v/+TZRmkHomUIaAaSSVBmcmZiPTKeQYAtIDys4A14lzY5gs2cQAAAAASUVORK5CYII=";
    static OPEN_ERROR_MESSAGE = "Unable to open popup.";
    static HEADER_EDIT_LABEL_PREFIX = "Edit ";
    static HEADER_ADD_LABEL_PREFIX = "Add ";
    constructor() {
        if (this.constructor == Popup) {
            throw new Error("Abstract classes can't be instantiated.");
        }
        this.header = null;
        this.closeButton = null;
    }

    isOpen() {
        throw new Error("Method 'isOpen()' must be implemented.");
    }

    getData() {
        throw new Error("Method 'getData()' must be implemented.");
    }

    getType() {
        return this.header.textContent
            .replace(Popup.HEADER_ADD_LABEL_PREFIX, "")
            .replace(Popup.HEADER_EDIT_LABEL_PREFIX, "");
    }

    close() {
        this.closeButton.click();
    }
}

class PasswordPopup extends Popup {
    static CONTAINER_SELECTOR = "#reactOverlayContent";
    static CLASS_IF_VISIBLE = "absolute";
    static CLOSE_BUTTON = "[data-automation-id='save-site-close-button']";
    static CONTENT = "[data-automation-id='save-site-dialog']";
    static ADVANCE_SETTINGS = "img[alt='Advance dropdown closed']";
    static CHECKBOXES = "button[name='passwordProtected'],button[name='autoLogin'],button[name='autoFill']";
    static FAVORITE_ICON = "button[data-automation-id='save-site-favourite-button'] img";
    static FAVORITE_ICON_CHECKED = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAATCAYAAACQjC21AAAABGdBTUEAALGPC/xhBQAAAmNJREFUOBFjYCACOHgH6oAwEUoZWIhR9J+JMQyq7goh9UyEFIDkWZmZIkGYGLUEDXRwC9ZgZfsvCcIgNiFDCRr4m5khzMXqCwsIg9gUG8jJwRDjYv2NHYRBbIoMtPEKVGNl/i+to/aTAYRBbJAYPkNZAmID0t5/YKlgYfrPia7w339GDmerL8yMjBAZZ6tvzFsP8Jx09g/+ga72zz/G74ICfzrASq09gyzY2RlXa6v+EstPeMfGwf4frl6A7y8DFweE/+0HI8OHT8xwuR8/GRkmLhD6dfU226ufP/+HHt2+7gTUbgYGbYdQHlHe/9OFBP4Etha/4VZX+gXXiI1x8x4bQ3WvyNd3H1jWv/7MmHn1wOovIHVwA2Ga7HyCwlmYmWYlhnzgivT9xMKElg7+/WNgWL6Z78/8NQLf/vz9l3Zoy7qVML1YDQQJ2rn5yzKys96YXP+CS0cN1aWXb7Iz5DWJf/v/87fGoV0bHyMbBmJjzXoszIxswITMoKWCahhIg7bqTwYOoNzvP4xsID46QPMQRPo3I0uYo8VXJpB3vwMjonWayHcQBrFBYiA5kBp0w0B8rAaysTHEuNp85bhxl40hukj6675jXKtBGMQGiYHkQGqwGYhIA1BZG5dgJTYOhhox4T//O2aIfP36lSHl4Ja1rQ9uXtsgJqN1Z8dhXncJkT9MV26zC0hJay1/dO/6e2SDMV3Iyhj6/QcTx6L1/Ke+f2bUOrRt7SqYBhAbJAaWA6phAKqFyeGknQODz9p6h1QBFWAkKSRNjCA1ILVIYphMS8tQTrdgf3NMGewiILUgPciyAOxq0kGvBvtPAAAAAElFTkSuQmCC";

    constructor() {
        super();
        this.container = getNode(PasswordPopup.CONTAINER_SELECTOR);
        this.containerShadow = this.container.childNodes[0].shadowRoot;
        this.content = this.containerShadow.querySelector(PasswordPopup.CONTENT);
        this.header = this.content.childNodes[0].childNodes[0];
        this.closeButton = this.containerShadow.querySelector(PasswordPopup.CLOSE_BUTTON);
    }

    getData() {
        let data = {};
        let obj = this;
        ["loginUrl", "name", "folder", "username", "password", "notes"].forEach(inputName => {
            data[inputName] = obj.content.querySelector(`[name="${inputName}"]`).value;
        });
        this.content.querySelector(PasswordPopup.ADVANCE_SETTINGS).parentNode.parentElement.click();
        this.content.querySelectorAll(PasswordPopup.CHECKBOXES).forEach(htmlButton => {
            data[htmlButton.attributes.name.value] = htmlButton.childNodes[0].attributes.src.value == PasswordPopup.IMAGE_SRC_CHECKBOX_CHECKED;
        });
        data["favorite"] = this.content.querySelector(PasswordPopup.FAVORITE_ICON).attributes.src.value == PasswordPopup.FAVORITE_ICON_CHECKED;
        return data;
    }

    isOpen() {
        let obj = this;
        return new Promise((resolve, reject) => {
            let tries = 0;
            let i = setInterval(() => {
                if(tries == ENVIRONMENT.INTERVAL_MAX_TRIES) reject(Popup.OPEN_ERROR_MESSAGE);
                if(obj.container.classList.contains(PasswordPopup.CLASS_IF_VISIBLE)) {
                    clearInterval(i);
                    resolve(true);
                }
                tries++;
            }, ENVIRONMENT.INTERVAL_MILLISECONDS);
        });
    }
}

class NotePopup extends Popup {
    static CONTAINER_SELECTOR = "#noteDialog";
    static DISPLAY_STYLE_IF_VISIBLE = "block";
    static HEADER = "#noteDialogTitle";
    static CLOSE_BUTTON = "#dialogCloseButton";
    static FIELD = "[dialogfield]";
    static INPUT_ROW = "tr.settingsRow";
    static INPUT_ROW_LABEL = "td.settingLabel";
    static INPUT_ROW_FIELD = ".dialogInput";
    static IGNORE_DIALOGFIELD = ["addAttachmentFileField", "notetype", "language"];
    static PASSWORD_PROTECTED_OLD = "pwprotect";
    static PASSWORD_PROTECTED_NEW = "passwordProtected";
    static DEFAULT_LABEL = "Notes";
    static FAVORITE_ICON = ".favButton";
    constructor() {
        super();
        this.container = getNode(NotePopup.CONTAINER_SELECTOR);
        this.header = this.container.querySelector(NotePopup.HEADER);
        this.closeButton = this.container.querySelector(NotePopup.CLOSE_BUTTON);
    }

    getData() {
        let data = {};
        this.container.querySelectorAll(NotePopup.FIELD).forEach(htmlInput => {
            if(NotePopup.IGNORE_DIALOGFIELD.indexOf(htmlInput.attributes.dialogfield.value) >= 0) return;
            if(htmlInput.tagName == "INPUT") {
                if(htmlInput.attributes.type.value == "checkbox") {
                    if(htmlInput.attributes.dialogfield.value == NotePopup.PASSWORD_PROTECTED_OLD)
                        data[NotePopup.PASSWORD_PROTECTED_NEW] = htmlInput.checked;
                    else 
                        data[htmlInput.attributes.dialogfield.value] = htmlInput.checked;
                    return;
                }
            }
            data[htmlInput.attributes.dialogfield.value] = htmlInput.value;
        });
        this.container.querySelectorAll(NotePopup.INPUT_ROW).forEach(row => {
            let htmlInputs = row.querySelectorAll(NotePopup.INPUT_ROW_FIELD);
            let value = "";
            
            if(htmlInputs.length == 1 && (htmlInputs[0].tagName == "INPUT" || 
                htmlInputs[0].tagName == "SELECT" || 
                htmlInputs[0].tagName == "TEXTAREA")) {
                value = htmlInputs[0].value;
            } else if(htmlInputs.length == 2 && htmlInputs[0].tagName == "SELECT" && 
                    htmlInputs[1].classList.contains("dateInputYear")) {
                value = {
                    "month": htmlInputs[0].value,
                    "year": htmlInputs[1].value
                };
            } else if(htmlInputs.length == 3 && htmlInputs[0].tagName == "SELECT" && 
                    htmlInputs[1].classList.contains("dateInputDay") && 
                    htmlInputs[2].classList.contains("dateInputYear")) {
                value = {
                    "month": htmlInputs[0].value,
                    "day": htmlInputs[1].value,
                    "year": htmlInputs[2].value
                };
            } else if(htmlInputs.length == 4 && htmlInputs[0].tagName == "SELECT" && 
                    htmlInputs[1].classList.contains("countryCode") && 
                    htmlInputs[2].classList.contains("phone")) {
                value = {
                    "country": htmlInputs[0].value,
                    "countryCode": htmlInputs[1].value,
                    "phone": htmlInputs[2].value,
                    "ext": htmlInputs[3].value
                };
            }
            data[row.querySelector(NotePopup.INPUT_ROW_LABEL) == null ? 
                NotePopup.DEFAULT_LABEL : 
                row.querySelector(NotePopup.INPUT_ROW_LABEL).textContent
            ] = value;
        })
        data["favorite"] = this.container.querySelector(NotePopup.FAVORITE_ICON).checked;
        return data;
    }

    isOpen() {
        let obj = this;
        return new Promise((resolve, reject) => {
            let tries = 0;
            let i = setInterval(() => {
                if(tries == ENVIRONMENT.INTERVAL_MAX_TRIES) reject(Popup.OPEN_ERROR_MESSAGE);
                if(obj.container.style.display == NotePopup.DISPLAY_STYLE_IF_VISIBLE) {
                    clearInterval(i);
                    resolve(true);
                }
                tries++;
            }, ENVIRONMENT.INTERVAL_MILLISECONDS);
        });
    }
}


function pause(second) {
    return new Promise(resolve => {
        setTimeout(function() {
            resolve();
        }, second != null ? second : ENVIRONMENT.PAUSE_SECOND * 1000)
    });
}

let lastPassExportObject = new LastPassExport();
lastPassExportObject.run();